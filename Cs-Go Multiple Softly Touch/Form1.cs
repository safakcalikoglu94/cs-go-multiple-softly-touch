﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;

namespace Cs_Go_Multiple_Softly_Touch
{
	public partial class Form1 : Form
	{
		// public static int idealPerformance = 350;
		public const Int32 precache_bayonet_ct = 87;
		public const Int32 precache_bayonet_t = 63;

		public const Int32 m_iViewModelIndex = 0x3240;
		public const Int32 m_nModelIndex = 0x258;
		public const Int32 m_hViewModel = 0x32F8;

		static int playerAdress;
		public Form1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{

			Classm.OpenProcess(ModuleBase.p.Id);
			clientModulelabel.Text = "Client Module: " + ModuleBase.clientModule.moduleAddress.ToString();
			engineModulelabel.Text = "Engine Module: " + ModuleBase.engineModule.moduleAddress.ToString();
			this.Text = nameChange(30);

			Thread antiFlash = new Thread(new ThreadStart(dwordAntiFlash));
			antiFlash.Start();

			Thread wh = new Thread(new ThreadStart(wall));
			wh.Start();

			Thread ch = new Thread(new ThreadStart(chams));
			ch.Start();

			Thread rdr = new Thread(new ThreadStart(radar));
			rdr.Start();

			Thread knifeC = new Thread(new ThreadStart(knifeChanger));
			knifeC.Start();

			Thread skinC = new Thread(new ThreadStart(skinChanger));
			skinC.Start();
		}
		private void titleChanger(object sender, EventArgs e)
		{
			this.Text = nameChange(30);
		}

		private static string nameChange(int length)
		{
			StringBuilder builder = new StringBuilder();
			Random random = new Random();
			string input = "#$!%&?}{][ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
			var chars = Enumerable.Range(0, length).Select(x => input[random.Next(0, input.Length)]);
			return new string(chars.ToArray());
		}

		public static float flashMaxAlpha
		{
			get
			{
				int BaseAddress = Classm.ReadInt((int)ModuleBase.clientModule.moduleAddress + signatures.dwLocalPlayer);
				return Classm.ReadFloat((int)BaseAddress + Offsets.m_flFlashMaxAlpha);
				//  return Classm.ReadFloat((int)ModuleBase.BaseAddress + Offsets.m_flFlashMaxAlpha);
			}
			set
			{
				int BaseAddress = Classm.ReadInt((int)ModuleBase.clientModule.moduleAddress + signatures.dwLocalPlayer);
				Classm.WriteFloat((int)BaseAddress + Offsets.m_flFlashMaxAlpha, (float)value);
				// Classm.WriteFloat((int)ModuleBase.BaseAddress + Offsets.m_flFlashMaxAlpha, (float)value);
			}
		}

		public static void dwordAntiFlash()
		{
			if (ModuleBase.engineModule.moduleAddress != 0x000000)
			{

				// Flash durumu
				float flashVal = Classm.ReadFloat((int)ModuleBase.BaseAddress + Offsets.m_flFlashMaxAlpha);
				while (true)
				{
					int EngineBase = Classm.ReadInt(ModuleBase.engineModule.moduleAddress + signatures.dwClientState);
					int GameState = Classm.ReadInt(EngineBase + signatures.dwClientState);
					if (HackStatus.antiflashStatus == true)
					{
						if (GameState != 0)
						{
							// MessageBox.Show(GameState.ToString());
							//   MessageBox.Show("gamestate: "+GameState.ToString());
							//   MessageBox.Show("player: "+ Player.playerAdress.ToString());
							//    Classm.WriteFloat(Player.playerAdress + Offsets.m_flFlashMaxAlpha, (float)0);
							flashMaxAlpha = 100f;
							//   MessageBox.Show("flash val: " + flashVal.ToString()); Thread.Sleep(350);
						}
					}
					else
					{
						flashMaxAlpha = 255f;

					}
					Thread.Sleep(350);
				}
			}
		}
		public class HackStatus
		{
			public static bool antiflashStatus = false;
			public static bool whstatus = false;
			public static bool chms = false;
			public static bool rdr = false;
			public static bool skin = false;
		}

		private void noFlash_CheckedChanged(object sender, EventArgs e)
		{
			if (noFlash.Checked == true)
			{
				HackStatus.antiflashStatus = true;
				noFlashlabel.Text = HackStatus.antiflashStatus.ToString();
			}
			else
			{
				HackStatus.antiflashStatus = false;
				noFlashlabel.Text = HackStatus.antiflashStatus.ToString();
			}
		}

		public struct glow
		{
			public float r,
			g,
			b,
			a;
			public Boolean aq,
			aw;

		}

		public static void wall()
		{
			while (true)
			{
				int EngineBase = Classm.ReadInt(ModuleBase.engineModule.moduleAddress + signatures.dwClientState);
				int GameState = Classm.ReadInt(EngineBase + signatures.dwClientState);
				//gamestate durumuna göre girecek
				if (HackStatus.whstatus == true)
				{
					//bulunduğumuz takımın renkleri
					glow team = new glow()
					{
						r = 0,
						g = 0,
						b = 1,
						a = 1,
						aq = true,
						aw = false

					};

					glow enemy = new glow()
					{
						r = 1,
						g = 0,
						b = 0,
						a = 1,
						aq = true,
						aw = false

					};

					for (int i = 0; i <= 64; i++)
					{

						playerAdress = Classm.ReadInt(ModuleBase.clientModule.moduleAddress + signatures.dwLocalPlayer);
						int myTeam = Classm.ReadInt(playerAdress + Offsets.m_iTeamNum);
						int entityList = Classm.ReadInt(ModuleBase.clientModule.moduleAddress + signatures.dwEntityList + (i) * 0x10);
						int enemyTeam = Classm.ReadInt(entityList + Offsets.m_iTeamNum);
						if (!Classm.ReadBool((entityList + signatures.m_bDormant)))
						{
							int glowindex = Classm.ReadInt(entityList + Offsets.m_iGlowIndex);
							int glowObject = Classm.ReadInt(ModuleBase.clientModule.moduleAddress + signatures.dwGlowObjectManager);

							if (myTeam != enemyTeam)
							{
								Classm.WriteFloat((glowObject + ((glowindex * 0x38) + 0x4)), enemy.r);
								Classm.WriteFloat((glowObject + ((glowindex * 0x38) + 0x8)), enemy.g);
								Classm.WriteFloat((glowObject + ((glowindex * 0x38) + 0xC)), enemy.b);
								Classm.WriteFloat((glowObject + ((glowindex * 0x38) + 0x10)), enemy.a);
								Classm.WriteBoolean((glowObject + ((glowindex * 0x38) + 0x24)), enemy.aq);
								//  Classm.WriteFloat((glowObject + ((glowindex * 0x38) + 0x2c)), 100);
								//  Classm.WriteBoolean((glowObject + ((glowindex * 0x38) + 0x25)), enemy.aq);
							}
						}

					}
				}
				Thread.Sleep(1);
			}

		}
		public static void setSpotted(int player)
		{
			Classm.WriteBoolean(player + Offsets.m_bSpotted, true);

		}

		public static void radar()
		{
			while (true)
			{
				if (HackStatus.rdr == true)
				{
					for (int i = 0; i <= 64; i++)
					{
						playerAdress = Classm.ReadInt(ModuleBase.clientModule.moduleAddress + signatures.dwLocalPlayer);
						int myTeam = Classm.ReadInt(playerAdress + Offsets.m_iTeamNum);
						int entityList = Classm.ReadInt(ModuleBase.clientModule.moduleAddress + signatures.dwEntityList + (i) * 0x10);
						int enemyTeam = Classm.ReadInt(entityList + Offsets.m_iTeamNum);
						if (myTeam != enemyTeam && Classm.ReadBool(entityList + signatures.m_bDormant) == false)
						{
							setSpotted(entityList);
						}
					}					
				}
				Thread.Sleep(1);
			}
		}
		public static void chams()
		{
			while (true)
			{
				if (HackStatus.chms == true) //gamestate durumuna göre girecek
				{
					for (int i = 0; i <= 64; i++)
					{
						playerAdress = Classm.ReadInt(ModuleBase.clientModule.moduleAddress + signatures.dwLocalPlayer);
						int myTeam = Classm.ReadInt(playerAdress + Offsets.m_iTeamNum);
						int entityList = Classm.ReadInt(ModuleBase.clientModule.moduleAddress + signatures.dwEntityList + (i) * 0x10);
						int enemyTeam = Classm.ReadInt(entityList + Offsets.m_iTeamNum);
						if (myTeam != enemyTeam)
						{
							Classm.WriteByte(entityList + Offsets.m_clrRender, 0);
							Classm.WriteByte(entityList + Offsets.m_clrRender + 0x01, 255);
							Classm.WriteByte(entityList + Offsets.m_clrRender + 0x02, 0);
							Classm.WriteByte(entityList + Offsets.m_clrRender + 0x03, 255);

							float brightness = 10f;
							int thisPtr = (int)(ModuleBase.engineModule.moduleAddress + signatures.model_ambient_min - 0x2c);
							byte[] bytearray = BitConverter.GetBytes(brightness);
							int intbrightness = BitConverter.ToInt32(bytearray, 0);
							int xored = intbrightness ^ thisPtr;
							Classm.WriteInt(ModuleBase.engineModule.moduleAddress + signatures.model_ambient_min, xored);

						}

					}
				}
				Thread.Sleep(350);
			}
		}
		private void Form1_FormClosed(object sender, FormClosedEventArgs e)
		{
			Environment.Exit(0);
		}

		private void checkboxwh_CheckedChanged(object sender, EventArgs e)
		{
			if (checkboxwh.Checked == true)
			{
				HackStatus.whstatus = true;
				walllabel.Text = HackStatus.whstatus.ToString();
			}
			else
			{
				HackStatus.whstatus = false;
				walllabel.Text = HackStatus.whstatus.ToString();
			}
		}

		private void checkBoxrdr_CheckedChanged(object sender, EventArgs e)
		{
			if (checkBoxrdr.Checked == true)
			{
				HackStatus.rdr = true;
				radarlabel.Text = HackStatus.rdr.ToString();
			}
			else
			{
				HackStatus.rdr = false;
				radarlabel.Text = HackStatus.rdr.ToString();

			}
		}

		private void checkboxchms_CheckedChanged(object sender, EventArgs e)
		{
			if (checkboxchms.Checked == true)
			{
				HackStatus.chms = true;
				chamslabel.Text = HackStatus.chms.ToString();
			}
			else
			{

				HackStatus.chms = false;
				chamslabel.Text = HackStatus.chms.ToString();
			}
		}

		private void checkBoxSkin_CheckedChanged(object sender, EventArgs e)
		{
			if (checkBoxSkin.Checked == true)
			{
				HackStatus.skin = true;
				labelSkin.Text = HackStatus.skin.ToString();
			}
			else
			{
				HackStatus.skin = false;
				labelSkin.Text = HackStatus.skin.ToString();
			}
		}

		public static void knifeChanger()
		{
			short weaponID = 0;
			short itemdef = 507; ;
			const float fallbackWear = 0.0001f;
			const int itemIDHigh = -1;
			int WeaponKnifeT = 59;
			int WeaponKnife = 42;
			const int entityQuality = 3;
			int paintkit = 561;
			int modelIndex = 0;
			playerAdress = Classm.ReadInt(ModuleBase.clientModule.moduleAddress + signatures.dwLocalPlayer);
			while (true)
			{
				if (HackStatus.skin == true)
				{
					if (modelIndex > 0)
					{

						for (int i = 0; i <= 8; i++)
						{

							int currentWeapon = Classm.ReadInt(playerAdress + Offsets.m_hMyWeapons + i * 0x4) & 0xfff;
							int weaponEntity = Classm.ReadInt(ModuleBase.clientModule.moduleAddress + signatures.dwEntityList + (currentWeapon - 1) * 0x10);
							weaponID = Classm.ReadShort(weaponEntity + Offsets.m_iItemDefinitionIndex);


							if (weaponID != WeaponKnife && weaponID != WeaponKnifeT)
							{
								continue;
							}
							else
							{
								Classm.WriteShort(weaponEntity + Offsets.m_iItemDefinitionIndex, itemdef);
								Classm.WriteInt(weaponEntity + m_nModelIndex, modelIndex);
								Classm.WriteInt(weaponEntity + m_iViewModelIndex, modelIndex);
								Classm.WriteInt(weaponEntity + Offsets.m_iEntityQuality, entityQuality);
							}
							Classm.WriteInt(weaponEntity + Offsets.m_iItemIDHigh, itemIDHigh);
							Classm.WriteInt(weaponEntity + Offsets.m_nFallbackPaintKit, paintkit);
							Classm.WriteFloat(weaponEntity + Offsets.m_flFallbackWear, fallbackWear);
						}
					}
					int activeWeapon = Classm.ReadInt(playerAdress + Offsets.m_hActiveWeapon) & 0xfff;
					int activeWeapone = Classm.ReadInt(ModuleBase.clientModule.moduleAddress + signatures.dwEntityList + (activeWeapon - 1) * 0x10);

					if (activeWeapone == 0)
					{
						continue;
					}

					weaponID = Classm.ReadShort(activeWeapone + Offsets.m_iItemDefinitionIndex);

					// int weaponViewModelID =Classm.ReadInt( activeWeapone + m_iViewModelIndex);

					if (weaponID == WeaponKnife)
					{
						modelIndex = 507;
					}
					else if (weaponID == WeaponKnifeT)
					{
						modelIndex = 507;
					}
					else if (weaponID != itemdef || modelIndex == 0)
					{
						continue;
					}

					int knifeViewModel = Classm.ReadInt(playerAdress + m_hViewModel) & 0xfff;
					int knifeViewModell = Classm.ReadInt(ModuleBase.clientModule.moduleAddress + signatures.dwEntityList + (knifeViewModel - 1) * 0x10);
					if (knifeViewModell == 0)
					{
						continue;
					}

					Classm.WriteInt(knifeViewModell + m_nModelIndex, modelIndex);

				}
			}
		}



		public static void skinChanger()
		{
			while (true)
			{

				if (HackStatus.skin == true)
				{
					const int itemIDHigh = -1;
					const float fallbackWear = 0.0001f;

					for (int i = 1; i <= 7; i++)
					{
						int PaintKit = 0;
						playerAdress = Classm.ReadInt(ModuleBase.clientModule.moduleAddress + signatures.dwLocalPlayer);
						int curWeaponIndex = Classm.ReadInt((playerAdress + Offsets.m_hMyWeapons + ((i - 1) * 0x4))) & 0xfff;
						int curWeaponEnt = Classm.ReadInt((ModuleBase.clientModule.moduleAddress + signatures.dwEntityList + (curWeaponIndex - 1) * 0x10));
						short curWeaponID = (short)Classm.ReadInt((curWeaponEnt + Offsets.m_iItemDefinitionIndex));
						if (curWeaponID != 0)
						{

							//	if (curWeaponID == 1)
							//	{ MessageBox.Show(curWeaponID.ToString()); }
							switch (curWeaponID)
							{
								case 1:
									PaintKit = 37; //Deagle Blaze
									break;
								case 3:
									PaintKit = 660; //Five-Seven HyperBeast
									break;
								case 4:
									PaintKit = 38; //Glock-Fade
									break;
								case 7:
									PaintKit = 675; //AK-Vulcan
									break;
								case 8:
									PaintKit = 455; //AUG Weeaboo
									break;
								case 9:
									PaintKit = 344; //AWP Dragon Lore
									break;
								case 10:
									PaintKit = 919; //Famas Styx
									break;
								case 13:
									PaintKit = 398; //Galil Eco
									break;
								case 16:
									PaintKit = 309; //M4A4 Howl
									break;
								case 17:
									PaintKit = 433; //Mac-10 Neon Rider
									break;
								case 19:
									PaintKit = 359; //P90 Asiimov
									break;
								case 24:
									PaintKit = 556; //UMP Primal Saber
									break;
								case 30:
									PaintKit = 644; //Tec-9 Decimator
									break;
								case 34:
									PaintKit = 734; //MP9 Wild Lily
									break;
								case 36:
									PaintKit = 404; //P250 Muertos
									break;
								case 39:
									PaintKit = 487; //SG Cyrex
									break;
								case 40:
									PaintKit = 624; //SSG Dragonfire
									break;
								case 61:
									PaintKit = 504; //USPS Kill Confirmed
									break;
							}
							Classm.WriteInt(curWeaponEnt + Offsets.m_OriginalOwnerXuidLow, 0);
							Classm.WriteInt(curWeaponEnt + Offsets.m_OriginalOwnerXuidHigh, 0);
							Classm.WriteInt(curWeaponEnt + Offsets.m_iItemIDHigh, itemIDHigh);
							Classm.WriteInt(curWeaponEnt + Offsets.m_nFallbackPaintKit, PaintKit);
							//	MessageBox.Show(Classm.ReadInt(curWeaponEnt + Offsets.m_nFallbackPaintKit).ToString());
							Classm.WriteFloat(curWeaponEnt + Offsets.m_flFallbackWear, fallbackWear);
						}

					}

				}
			}

		}

	}
}