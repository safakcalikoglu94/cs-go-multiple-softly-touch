﻿namespace Cs_Go_Multiple_Softly_Touch
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.clientModulelabel = new System.Windows.Forms.Label();
            this.engineModulelabel = new System.Windows.Forms.Label();
            this.noFlash = new System.Windows.Forms.CheckBox();
            this.noFlashlabel = new System.Windows.Forms.Label();
            this.walllabel = new System.Windows.Forms.Label();
            this.checkboxwh = new System.Windows.Forms.CheckBox();
            this.checkboxchms = new System.Windows.Forms.CheckBox();
            this.chamslabel = new System.Windows.Forms.Label();
            this.checkBoxrdr = new System.Windows.Forms.CheckBox();
            this.radarlabel = new System.Windows.Forms.Label();
            this.checkBoxSkin = new System.Windows.Forms.CheckBox();
            this.labelSkin = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // clientModulelabel
            // 
            this.clientModulelabel.AutoSize = true;
            this.clientModulelabel.Location = new System.Drawing.Point(39, 8);
            this.clientModulelabel.Name = "clientModulelabel";
            this.clientModulelabel.Size = new System.Drawing.Size(89, 13);
            this.clientModulelabel.TabIndex = 0;
            this.clientModulelabel.Text = "clientModulelabel";
            // 
            // engineModulelabel
            // 
            this.engineModulelabel.AutoSize = true;
            this.engineModulelabel.Location = new System.Drawing.Point(36, 26);
            this.engineModulelabel.Name = "engineModulelabel";
            this.engineModulelabel.Size = new System.Drawing.Size(77, 13);
            this.engineModulelabel.TabIndex = 1;
            this.engineModulelabel.Text = " engineModule";
            // 
            // noFlash
            // 
            this.noFlash.AutoSize = true;
            this.noFlash.Location = new System.Drawing.Point(39, 44);
            this.noFlash.Name = "noFlash";
            this.noFlash.Size = new System.Drawing.Size(63, 17);
            this.noFlash.TabIndex = 2;
            this.noFlash.Text = "no flash";
            this.noFlash.UseVisualStyleBackColor = true;
            this.noFlash.CheckedChanged += new System.EventHandler(this.noFlash_CheckedChanged);
            // 
            // noFlashlabel
            // 
            this.noFlashlabel.AutoSize = true;
            this.noFlashlabel.Location = new System.Drawing.Point(108, 45);
            this.noFlashlabel.Name = "noFlashlabel";
            this.noFlashlabel.Size = new System.Drawing.Size(29, 13);
            this.noFlashlabel.TabIndex = 3;
            this.noFlashlabel.Text = "false";
            // 
            // walllabel
            // 
            this.walllabel.AutoSize = true;
            this.walllabel.Location = new System.Drawing.Point(108, 69);
            this.walllabel.Name = "walllabel";
            this.walllabel.Size = new System.Drawing.Size(29, 13);
            this.walllabel.TabIndex = 7;
            this.walllabel.Text = "false";
            // 
            // checkboxwh
            // 
            this.checkboxwh.AutoSize = true;
            this.checkboxwh.Location = new System.Drawing.Point(39, 68);
            this.checkboxwh.Name = "checkboxwh";
            this.checkboxwh.Size = new System.Drawing.Size(68, 17);
            this.checkboxwh.TabIndex = 8;
            this.checkboxwh.Text = "wallhack";
            this.checkboxwh.UseVisualStyleBackColor = true;
            this.checkboxwh.CheckedChanged += new System.EventHandler(this.checkboxwh_CheckedChanged);
            // 
            // checkboxchms
            // 
            this.checkboxchms.AutoSize = true;
            this.checkboxchms.Location = new System.Drawing.Point(39, 91);
            this.checkboxchms.Name = "checkboxchms";
            this.checkboxchms.Size = new System.Drawing.Size(57, 17);
            this.checkboxchms.TabIndex = 11;
            this.checkboxchms.Text = "chams";
            this.checkboxchms.UseVisualStyleBackColor = true;
            this.checkboxchms.CheckedChanged += new System.EventHandler(this.checkboxchms_CheckedChanged);
            // 
            // chamslabel
            // 
            this.chamslabel.AutoSize = true;
            this.chamslabel.Location = new System.Drawing.Point(108, 93);
            this.chamslabel.Name = "chamslabel";
            this.chamslabel.Size = new System.Drawing.Size(29, 13);
            this.chamslabel.TabIndex = 12;
            this.chamslabel.Text = "false";
            // 
            // checkBoxrdr
            // 
            this.checkBoxrdr.AutoSize = true;
            this.checkBoxrdr.Location = new System.Drawing.Point(39, 114);
            this.checkBoxrdr.Name = "checkBoxrdr";
            this.checkBoxrdr.Size = new System.Drawing.Size(50, 17);
            this.checkBoxrdr.TabIndex = 13;
            this.checkBoxrdr.Text = "radar";
            this.checkBoxrdr.UseVisualStyleBackColor = true;
            this.checkBoxrdr.CheckedChanged += new System.EventHandler(this.checkBoxrdr_CheckedChanged);
            // 
            // radarlabel
            // 
            this.radarlabel.AutoSize = true;
            this.radarlabel.Location = new System.Drawing.Point(108, 115);
            this.radarlabel.Name = "radarlabel";
            this.radarlabel.Size = new System.Drawing.Size(29, 13);
            this.radarlabel.TabIndex = 14;
            this.radarlabel.Text = "false";
            // 
            // checkBoxSkin
            // 
            this.checkBoxSkin.AutoSize = true;
            this.checkBoxSkin.Location = new System.Drawing.Point(39, 139);
            this.checkBoxSkin.Name = "checkBoxSkin";
            this.checkBoxSkin.Size = new System.Drawing.Size(90, 17);
            this.checkBoxSkin.TabIndex = 15;
            this.checkBoxSkin.Text = "Skin Changer";
            this.checkBoxSkin.UseVisualStyleBackColor = true;
            this.checkBoxSkin.CheckedChanged += new System.EventHandler(this.checkBoxSkin_CheckedChanged);
            // 
            // labelSkin
            // 
            this.labelSkin.AutoSize = true;
            this.labelSkin.Location = new System.Drawing.Point(127, 141);
            this.labelSkin.Name = "labelSkin";
            this.labelSkin.Size = new System.Drawing.Size(29, 13);
            this.labelSkin.TabIndex = 16;
            this.labelSkin.Text = "false";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(221, 175);
            this.Controls.Add(this.labelSkin);
            this.Controls.Add(this.checkBoxSkin);
            this.Controls.Add(this.radarlabel);
            this.Controls.Add(this.checkBoxrdr);
            this.Controls.Add(this.chamslabel);
            this.Controls.Add(this.checkboxchms);
            this.Controls.Add(this.checkboxwh);
            this.Controls.Add(this.walllabel);
            this.Controls.Add(this.noFlashlabel);
            this.Controls.Add(this.noFlash);
            this.Controls.Add(this.engineModulelabel);
            this.Controls.Add(this.clientModulelabel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.TopMost = true;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label clientModulelabel;
        private System.Windows.Forms.Label engineModulelabel;
        private System.Windows.Forms.CheckBox noFlash;
        private System.Windows.Forms.Label noFlashlabel;
        private System.Windows.Forms.Label walllabel;
        private System.Windows.Forms.CheckBox checkboxwh;
        private System.Windows.Forms.CheckBox checkboxchms;
        private System.Windows.Forms.Label chamslabel;
        private System.Windows.Forms.CheckBox checkBoxrdr;
        private System.Windows.Forms.Label radarlabel;
        private System.Windows.Forms.CheckBox checkBoxSkin;
        private System.Windows.Forms.Label labelSkin;
    }
}

